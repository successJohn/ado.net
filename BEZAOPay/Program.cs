﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BEZAOPayDAL;
using System.Configuration;
using BEZAOPayDAL.Models;


namespace BEZAOPay
{
    class Program
    {
        static void Main(string[] args)
        {
            var connection = ConfigurationManager.ConnectionStrings["BEZAOConnect"].ConnectionString;
            var db = new BEZAODAL();

            db.InsertUser(new User { Name = "baby", Email ="baby@gmail.com" });
            db.InsertUser(new User { Name = "boy", Email ="boy@gmail.com" }); 
            var users = db.GetAllUsers();

            foreach (var user in users)
            {
                Console.WriteLine($"Id: {user.Id}\nName: {user.Name}\nEmail: {user.Email}");
            }
        }
    }
}
